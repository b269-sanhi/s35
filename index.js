const express = require("express")

// Monggose is a package that allows creating of schemas to our model our data structures
// Also has access to a number of method for manipulating out database
const mongoose = require("mongoose")

const app = express()
const port = 3001

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://psanhi24:admin123@zuitt-bootcamp.asvy4zs.mongodb.net/s35?retryWrites=true&w=majority",
	{	
		// alloos us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

// allows to handle errors when the initial connection is establish
let db = mongoose.connection

// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "Coonection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"))
// If the connection is successful, out in the console
db.once("open", () => console.log("We're connected to the cloud database"))

// Connecting to MongoDB Atlas END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

//[SECTION] Mongoose Schemas

const taskSchema = new mongoose.Schema({
	name: String,
	email: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

// [SECTION] Mongose Models
// Model must be in singular form and capitalized
const Task = mongoose.model("Task", taskSchema)

// Creation of Task application
// Create a new task
/*
Business Logic
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/task", (req, res) =>{
	Task.findOne({name: req.body.name}) .then((result, err) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}else{
			let newTask = new Task({
				name: req.body.name
			})
			newTask.save().then((savedTask, saveErr) =>{
				if(saveErr){
					return console.log(saveErr)
				}else{
					return res.status(201).send("New task created!")
				}
			})
		}
	})
})

// Getting all the task
/*
BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/task", (req, res) =>{
	Task.find({}).then((result, err) => {
		if(err) {
			return console.log(err)
		}else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

// [SECTION] ACTIVITY

const userSchema = new mongoose.Schema({
	userName: String,
	password: String,
	status: {
		type: String,

	}
})


const User = mongoose.model("User", userSchema)




app.post("/signup", (req, res) =>{
	User.findOne({userName: req.body.userName}, {password: req.body.password}) .then((result, err) => {
		if(result != null && result.userName == req.body.userName && result.password == req.body.password ){
			return res.send("Duplicate user found")
		}else{
			let newUser = new User({
				userName: req.body.userName,
				password: req.body.password
			})
			newUser.save().then((savedTask, saveErr) =>{
				if(saveErr){
					return console.log(saveErr)
				}else{
					return res.status(201).send("New user registered!")
				}
			})
		}
	})
})




app.listen(port, () => console.log(`Server running at port ${port}`))